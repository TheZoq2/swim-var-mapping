# Var Mapping

A swim plugin that prints a mapping between spade expressions and yosys cells.

## Usage

Add the plugin to `swim.toml`

```
[plugins.var_map]
git = "git@gitlab.com:TheZoq2/swim-var-mapping.git"
branch = "main"
args.resource = "TRELLIS_FF"
args.out_port = "Q"
```

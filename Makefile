SPADE_INCLUDE_DIR=${SPADE_DIR}/target/cxxbridge/spade-cxx/src
SPADE_BUILD_DIR=${SPADE_DIR}/target/release

${BUILD_DIR}/var_mapping.so: var_mapping.cpp ${SPADE_BUILD_DIR}/libspade_cxx.a Makefile
	mkdir -p $(@D)
	yosys-config \
		--exec \
		--cxx \
		--cxxflags \
		--ldflags \
		-o $@ \
		-shared \
		--ldlibs \
		var_mapping.cpp \
		-I${SPADE_INCLUDE_DIR} \
		-lspade_cxx \
		-L${SPADE_BUILD_DIR} \
		-std=c++17

#include "kernel/yosys.h"
#include <lib.rs.h>

#include <map>

USING_YOSYS_NAMESPACE

struct MyPass : public Pass {
    MyPass() : Pass("var_mapping", "just a simple test") { }

    virtual void execute(std::vector<std::string> args, RTLIL::Design *design) {
        // rust_from_cpp();
        auto comp_state = read_state("build/state.ron");
        // comp_state->list_names();
        // log("Demangle test: %s\n", comp_state->demangle_name("_e_322").c_str());

        if (args.size() != 4) {
            log_error("Expected exactly 3 arguments to var_mapping");
        }
        //std::string top = "\\e_proj_vga2dvid_vga2dvid";
        std::string top = "\\" + args[1];
        std::string resource = "\\" + args[2];
        std::string output_port = "\\" + args[3];

        log("**USING TOP MODULE %s**\n", top.c_str());
        log("   Looking for %s with output port %s\n", resource.c_str(), output_port.c_str());
;
        std::map<std::string, int> ff_mapping;

        for (auto mod : design->modules()) {
            if (mod->name.str() == top) {
                for(auto cell : mod->cells()) {
                    if (cell->type == resource) {
                        // log("%s\n", cell->type.c_str());

                        auto port = cell->getPort(output_port);

                        if(port.is_chunk()) {
                            auto chunk = port.as_chunk();
                            auto wire = chunk.wire;

                            ff_mapping[wire->name.str()] += chunk.width;
                            // log("Found Q port: %s\n", wire->name.c_str());
                        }
                        else {
                            log("NOTE: %s Port was not a chunk", output_port.c_str());
                        }
                    }
                }
            }
        }


        log("\nVariables mapped into TRELLIS_FF:\n");
        for (auto [name, count] : ff_mapping) {
            // +1 because names start with \ and I'm very lazy this friday evening
            auto spade_source = comp_state->demangle_name(name.c_str()+1);
            log("  %s: %i bits\n", spade_source.c_str(), count);
        }
    }
} MyPass;

